<?php

/**
 * @file
 * MyTube module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Xss;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 *
 * @phpstan-ignore missingType.iterableValue,missingType.parameter
 */
function mytube_help($route_name): array {
  $output = [];
  switch ($route_name) {
    case 'help.page.mytube':
      $output[] = t('MyTube prevents iframe and other embedded content from automatically loading by replacing it with user-activated, locally-hosted thumbnails. This protects users\' privacy because it prevents automatic remote requests and tracking cookies (e.g. <a href="https://www.eff.org/deeplinks/2009/09/new-cookie-technologies-harder-see-and-remove-wide">Flash cookies</a>). Effectively, until the user clicks "Play" no remote sites will receive statistics about the user having visited your web page from user-embedded videos. Additionally, without automatically loading embedded content, content on your site will load faster, making it more accessible to users with old computers or slow internet connections. Since many of the embeds are modified to autoplay when finally loaded, users will not necessarily have to &quot;double-click&quot; to load videos, and the process will be transparent for those who are not technically-inclined or detail-oriented, especially when custom thumbnails are set.');
      $output[] = t('After installing the module, administrators should enable the MyTube filter for any <a href="@input_formats">input formats</a> that will allow embeds. It is <em>strongly</em> recommended that administrators also enable the "Limit allowed HTML tags and correct faulty HTML" filter and arrange it <em>before</em> MyTube, especially if relatively anonymous users are allowed to submit embed code; failure to do so may allow users to inject arbitrary scripts using embeds. The appropriate tags that you want to authorize users to embed, such as <code>&lt;iframe&gt;</code>, will have to be allowed; any tags or attributes you wish to restrict can be restricted without breaking MyTube\'s ability to filter other authorized tags. There is no need to enable the "Correct faulty and chopped off HTML" filter as the MyTube filter provides this functionality.', [
        '@input_formats' => Url::fromRoute('filter.admin_overview')->toString(),
      ]);
      $output[] = t('Beyond this initial setup, MyTube should require little if any configuration, but offers several <a href="@options">customizable options</a> for appearance on its settings page. All <code>&lt;embed&gt;</code>, <code>&lt;iframe&gt;</code> and <code>&lt;object&gt;</code> tags passed through the MyTube filter will be replaced with MyTube thumbnails. For individual videos, a custom thumbnail can be added by adding <code>thumb="$relative_directory"</code> to the first affected tag. Default thumbnails for YouTube&trade; and Vimeo&trade; videos are downloaded to <code>@filepath</code> automatically and cannot be overridden.', [
        '@options' => Url::fromRoute('mytube.admin_settings')->toString(),
        '@filepath' => 'sites/default/files/mytube',
      ]);
      array_walk($output, function (&$value, $key) {
        $value = ['#type' => 'html_tag', '#tag' => 'p', '#value' => $value];
      });
      $output[] = ['#type' => 'html_tag', '#tag' => 'h3', '#value' => t('Input format guidelines:')];
      $output[] = ['#markup' => _mytube_tips(TRUE)];
      break;
  }
  return $output;
}

/**
 * Implements hook_theme().
 *
 * @phpstan-ignore missingType.iterableValue
 */
function mytube_theme(): array {
  return [
    'mytube_instance' => [
      'template' => 'mytube-instance',
      'variables' => [
        'thumb' => NULL,
        'play_icon' => NULL,
        'width' => 0,
        'height' => 0,
        'img_height' => 0,
        'img_margin' => 0,
        'escaped_embed' => NULL,
        'privacy_url' => NULL,
        'privacy_text' => NULL,
        'enable_js_api' => FALSE,
      ],
    ],
  ];
}

/**
 * Callback for filter tips.
 *
 * @return \Drupal\Component\Render\MarkupInterface
 *   The filter help.
 */
function _mytube_tips(bool $long = FALSE) {
  if (!$long) {
    return t("Embeds like YouTube and iframes will be prevented from grabbing visitors' data without their permission.");
  }
  else {
    $output[] = t('Embed code, such as YouTube&trade; embeds, or anything with <code>&lt;embed&gt;</code>, <code>&lt;object&gt;</code>, or <code>&lt;iframe&gt;</code> in it (usually pasted from somewhere), will automatically be replaced with user-clickable thumbnails. An example of such code is that from a video sharing or hosting website, offering to allow visitors to &quot;share&quot; or &quot;embed&quot; the video they just watched. By replacing these tags with clickable thumbnails, which in turn replace themselves with the original tags when clicked on by a viewer, we can prevent the Flash content from automatically loading, which would raise certain <a href="https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy">privacy concerns</a>.');
    $thumbnails[] = t('By default, the correct thumbnails are automatically fetched from youtube.com for YouTube&trade; embeds, and stored locally so no remote request is made by a reader, and privacy is preserved. Any other source of embed must be specified by placing a <code>thumb=</code> attribute in the first tag. If no thumbnail is specified, the system default thumbnail, shown below, will be used instead. Adding the <code>thumb=</code> attribute tells Drupal where to find the image, and that image will be shown instead of the default thumbnail.');
    if (\Drupal::config('mytube.settings')->get('allow_remote_thumb')) {
      $thumbnails[] = t('You may use thumbnails from remote sites by entering in the full URL.');
    }
    else {
      $thumbnails[] = t('Thumbnails may not be linked from remote sites; all thumbnails must be hosted on this server.');
    }
    $thumbnails[] = t('Note, if a thumbnail would normally be downloaded automatically, such as for <code>youtube.com</code>, you cannot customize it. This is done to prevent deliberately misleading video thumbnails and cannot be overridden by an administrator.') . '</p>';
    $output[] = implode(' ', $thumbnails);
    $output[] = t("Depending on the webmaster's settings, you may be able to set a custom width and height in the same way. If no recognizable width or height is provided in the embed code, the system defaults <u>will override</u> whatever was in the original embed code.");
    $embed = '<iframe width="560" height="315" src="https://www.youtube.com/embed/VL3cetAodLc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    $output[] = t('Example: <code>%embed</code>', ['%embed' => $embed]);
    $output[] = mytube_process($embed);
    $output[] = t('All affected embedded objects will be appended with the caption shown above, alerting users that by clicking the thumbnail they are consenting to have remote content served and acknowledging the consequential privacy issues. The caption has a link pointing viewers to more information, and may also display where the content will be served from.');
    array_walk($output, function (&$value) {
      $value = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $value,
      ];
    });
    $output['#attached']['library'][] = 'mytube/mytube';
    return \Drupal::service('renderer')->render($output);
  }
}

/**
 * Backwards compatibility alias for mytube_process().
 *
 * @deprecated in mytube:7.x-1.3 and is removed from mytube:2.0.0. Use
 * mytube_process().
 * @see https://www.drupal.org/project/mytube/issues/3282535
 */
function _mytube_process(string $text): string {
  return mytube_process($text);
}

/**
 * API function and process callback for the MyTube filter.
 *
 * @param string $text
 *   The HTML string to filter.
 *
 * @return string
 *   The HTML string with embed, iframe and object elements transformed to
 *   click-to-load elements.
 */
function mytube_process($text) {
  $tags_to_replace = ['object', 'iframe', 'embed'];
  $dom_document = Html::load($text);
  $allow_custom_size = \Drupal::config('mytube.settings')->get('allow_custom_size');
  if (!$allow_custom_size) {
    $width = \Drupal::config('mytube.settings')->get('default_width') ?: 320;
    if (!is_numeric($width)) {
      throw new \UnexpectedValueException('Default width must be a number.');
    }
    $width = $width > 0 ? $width : 320;
    $height = \Drupal::config('mytube.settings')->get('default_height') ?: 240;
    if (!is_numeric($height)) {
      throw new \UnexpectedValueException('Default height must be a number.');
    }
    $height = $height > 0 ? $height : 240;
  }
  $elements_to_remove = [];
  foreach ($tags_to_replace as $tag) {
    $embeds = $dom_document->getElementsByTagName($tag);
    foreach ($embeds as $embed) {
      if (\Drupal::config('mytube.settings')->get('iframe_max_privacy')) {
        $embed->setAttribute('allow', "accelerometer 'none'; ambient-light-sensor 'none'; autoplay; battery 'none'; bluetooth 'none'; browsing-topics 'none'; camera 'none'; ch-ua 'none'; display-capture 'none'; domain-agent 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; gamepad 'none'; geolocation 'none'; gyroscope 'none'; hid 'none'; identity-credentials-get 'none'; idle-detection 'none'; keyboard-map 'none'; local-fonts 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; otp-credentials 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials-create 'none'; publickey-credentials-get 'none'; screen-wake-lock 'none'; serial 'none'; speaker-selection 'none'; sync-xhr 'none'; usb 'none'; web-share 'none'; window-management 'none'; xr-spatial-tracking 'none'");
        $embed->setAttribute('credentialless', 'credentialless');
        $embed->setAttribute('csp', 'sandbox allow-scripts allow-same-origin;');
        $embed->setAttribute('referrerpolicy', 'no-referrer');
        $embed->setAttribute('sandbox', 'allow-scripts allow-same-origin');
        $src = preg_replace('#^https://(www\.)?youtube\.com/#', 'https://www.youtube-nocookie.com/', $embed->getAttribute('src'));
        if (!is_string($src)) {
          throw new \UnexpectedValueException('Regular expression error.');
        }
        $embed->setAttribute('src', $src);
      }
      else {
        // Ensure content can autoplay after click.
        $allow = $embed->getAttribute('allow');
        if (strpos($allow, 'autoplay') === FALSE) {
          $embed->setAttribute('allow', $allow ? $allow . '; autoplay' : 'autoplay');
        }
      }
      if (!$allow_custom_size) {
        $embed->setAttribute('width', (string) $width);
        $embed->setAttribute('height', (string) $height);
        if ($style = $embed->getAttribute('style')) {
          $embed->setAttribute('style', "$style; width: {$width}px; height: {$height}px;");
        }
      }
      _mytube_process_instance($embed, $elements_to_remove);
    }
  }
  foreach ($elements_to_remove as $element) {
    $element->parentNode->removeChild($element);
  }
  return Html::serialize($dom_document);
}

/**
 * Processes an instance.
 *
 * @param DOMElement $embed
 *   Embed or iframe DOMElement to process.
 * @param DOMElement[] $elements_to_remove
 *   Array of DOMElements to delete.
 */
function _mytube_process_instance(DOMElement $embed, array &$elements_to_remove): void {
  global $is_https, $base_url;
  $protocol = $is_https ? 'https://' : 'http://';
  if (!$embed->ownerDocument instanceof DOMDocument) {
    throw new \InvalidArgumentException('Embed element must have an owner document.');
  }
  $embedded = $embed->ownerDocument->saveHTML($embed);
  $embed_url = _mytube_embed_url($embed);
  if (!$embed_url) {
    $embed_url = 'https://www.example.com/';
  }
  // Bug in parse_url causes problems for URL of form "//$HOST/$PATH" (defined
  // in RFC 3986).
  $parseable_embed_url = preg_replace('[^//]', $protocol, $embed_url);
  if (!is_string($parseable_embed_url)) {
    throw new \UnexpectedValueException('Regular expression error.');
  }
  $host = parse_url($parseable_embed_url, PHP_URL_HOST);
  if (is_null($host)) {
    $host = parse_url($base_url, PHP_URL_HOST);
  }
  if (!is_string($host)) {
    throw new \UnexpectedValueException('Host must be a string.');
  }
  $embed_domain = _mytube_toplevel($host);

  // If this video is from a trusted domain, let it through unmodified.
  $trusted_domains = \Drupal::config('mytube.settings')->get('trusted_domains');
  if ($trusted_domains) {
    if (!is_string($trusted_domains)) {
      throw new \UnexpectedValueException('Trusted domains must be a string.');
    }
    $trusted_domains = preg_split('/\R/', $trusted_domains, -1, PREG_SPLIT_NO_EMPTY);
    if (!is_array($trusted_domains)) {
      throw new \UnexpectedValueException('Regular expression error.');
    }
    if (in_array($embed_domain, $trusted_domains)) {
      return;
    }
  }

  // first, find the thumbnail.
  $thumb = _mytube_thumb($embed, $parseable_embed_url, $embed_domain);
  $width = \Drupal::config('mytube.settings')->get('default_width') ?: 320;
  if (!is_numeric($width)) {
    throw new \UnexpectedValueException('Default width must be a number.');
  }
  $height = \Drupal::config('mytube.settings')->get('default_height') ?: 240;
  if (!is_numeric($height)) {
    throw new \UnexpectedValueException('Default height must be a number.');
  }
  if (\Drupal::config('mytube.settings')->get('allow_custom_size')) {
    $width = (int) $embed->getAttribute('width') ?: $width;
    $height = (int) $embed->getAttribute('height') ?: $height;
  }
  // Determine the output html.
  $privacy_url = \Drupal::config('mytube.settings')->get('url');
  if ($privacy_url == '') {
    $privacy_url = 'https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy';
  }
  $privacy_text = \Drupal::config('mytube.settings')->get('text');
  if ($privacy_text == '') {
    $privacy_text = t('This embed will serve content from <a rel="nofollow noreferrer" href=":embed_url">%embed_domain</a>.', [
      ':embed_url' => $embed_url,
      '%embed_domain' => $embed_domain,
    ]);
  }
  else {
    if (!is_string($privacy_text)) {
      throw new \UnexpectedValueException('Privacy text must be a string.');
    }
    $privacy_text = str_replace('!embed', htmlspecialchars($embed_url), $privacy_text);
    $privacy_text = str_replace('!domain', htmlspecialchars($embed_domain), $privacy_text);
    $privacy_text = Xss::filterAdmin($privacy_text);
  }
  $play_icon = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::service('extension.path.resolver')->getPath('module', 'mytube') . '/play.png');

  // Modify the code-to-be-revealed if necessary.
  _mytube_fix_embedded($embedded, $embed_url, $embed_domain, (int) $width, (int) $height);

  $enable_js_api = FALSE;
  $img_height = $height;
  $img_margin = 0;
  if (preg_match('/^youtube(-nocookie)?\.com$/', $embed_domain)) {
    if (\Drupal::config('mytube.settings')->get('enable_js_api')) {
      $enable_js_api = TRUE;
    }
    // Use negative margin to hide letterbox on YouTube thumbnails.
    $img_height = $width * 0.75;
    if ($height != $img_height) {
      $img_margin = ($height - $img_height) * 0.5 . 'px 0';
    }
  }

  $renderable = [
    '#theme' => 'mytube_instance',
    '#thumb' => $thumb,
    '#play_icon' => $play_icon,
    '#width' => $width,
    '#height' => $height,
    '#img_height' => $img_height,
    '#img_margin' => $img_margin,
    '#escaped_embed' => rawurlencode($embedded),
    '#privacy_url' => $privacy_url,
    '#privacy_text' => $privacy_text,
    '#enable_js_api' => $enable_js_api,
  ];
  $out = Html::load(\Drupal::service('renderer')->render($renderable));
  if (!$out->documentElement instanceof DOMElement) {
    throw new \UnexpectedValueException('Failed to render instance as an element.');
  }
  $body = $out->documentElement->getElementsByTagName('body')->item(0);
  if (!$body instanceof DOMElement) {
    throw new \UnexpectedValueException('Failed to render instance as an element in a body.');
  }
  if (!$embed->parentNode instanceof DOMNode) {
    throw new \UnexpectedValueException('Failed to find parent of embed element.');
  }
  foreach ($body->childNodes as $node) {
    $embed->parentNode->insertBefore($embed->ownerDocument->importNode($node, TRUE), $embed);
  }
  $elements_to_remove[] = $embed;
}

/**
 * Find the URL an embed will pull data from.
 *
 * @param DOMElement $embed
 *   Embed element to check.
 *
 * @return string|false
 *   The embed URL, or false.
 */
function _mytube_embed_url(DOMElement $embed) {
  switch ($embed->tagName) {
    case 'embed':
    case 'iframe':
      if ($src = $embed->getAttribute('src')) {
        return $src;
      }
      break;

    case 'object':
      foreach ($embed->getElementsByTagName('embed') as $child) {
        if ($src = $child->getAttribute('src')) {
          return $src;
        }
      }
      foreach ($embed->getElementsByTagName('param') as $child) {
        if ($child->getAttribute('name') === 'movie' && ($value = $child->getAttribute('value'))) {
          return $value;
        }
      }
  }
  return FALSE;
}

/**
 * Return a domain name's top-level alias.
 *
 * E.g. "eff.org" instead of "www.eff.org".
 */
function _mytube_toplevel(string $host): string {
  $first_dot = strrpos($host, '.');
  if ($first_dot === FALSE) {
    return $host;
  }
  $chop = substr($host, 0, $first_dot);
  $second_dot = strrpos($chop, '.');
  if ($second_dot) {
    $out = substr($host, $second_dot + 1);
  }
  else {
    $out = $host;
  }
  if ($out == 'example.com') {
    return 'unknown';
  }
  else {
    return $out;
  }
}

/**
 * Find and/or fetch thumbnail for an embed.
 *
 * @param DOMElement $embed
 *   Embed markup.
 * @param string|false $url
 *   The URL or false.
 * @param string|false $domain
 *   The domain or false.
 */
function _mytube_thumb(DOMElement $embed, $url = FALSE, $domain = FALSE): string {
  $default = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::service('extension.path.resolver')->getPath('module', 'mytube') . '/default.gif');
  // No domain has been passed in, so let's parse it from url.
  if ($url && !$domain) {
    $url_info = parse_url($url);
    $domain = _mytube_toplevel($url_info['host'] ?? '');
  }
  if ($url && $domain && preg_match('/^(youtube(-nocookie)?|vimeo)\.com$/', $domain)) {
    // So let's fetch the thumbnail automatically.
    $uri = _mytube_download_thumb($url);
    if (!$uri) {
      $out = $default;
    }
    else {
      $out = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
      // Couldn't find thumbnail.
      if ($out == \Drupal::service('file_url_generator')->generateAbsoluteString('')) {
        $out = $default;
      }
    }
  }
  elseif ($thumb = $embed->getAttribute('thumb')) {
    $out = $thumb;
    if (UrlHelper::isExternal($out)) {
      // Check if user is allowed to use remote thumbs.
      if (!\Drupal::config('mytube.settings')->get('allow_remote_thumb')) {
        // If not, use included default.gif instead.
        $out = $default;
      }
    }
    else {
      $out = \Drupal::service('file_url_generator')->generateAbsoluteString($out);
    }
  }
  // No user-specified thumbnail, use a default image.
  else {
    $out = $default;
  }
  return $out;
}

/**
 * Returns thumbnail path for a supported video source.
 *
 * Fetches and processes new image if necessary.
 *
 * @return string|false
 *   The thumbnail path or false.
 */
function _mytube_download_thumb(string $url) {
  $url_info = parse_url($url);
  $toplevel = _mytube_toplevel($url_info['host'] ?? '');
  switch ($toplevel) {
    case 'youtube.com':
    case 'youtube-nocookie.com':
      $prefix = 'yt';
      if (!preg_match('{youtube(-nocookie)?.com/(v|embed)/([a-z0-9_-]+)}i', $url, $matches)) {
        return FALSE;
      }
      $vid = $matches[3];
      $fetch_me = "https://i.ytimg.com/vi/$vid/0.jpg";
      break;

    case 'vimeo.com':
      $prefix = 'v';
      if (strrpos($url, 'clip_id=')) {
        // Substring after first '?'.
        $flash_vars = preg_split('/\?/', $url);
        if (!is_array($flash_vars)) {
          throw new \UnexpectedValueException('Regular expression error.');
        }
        // Find clip_id=XXX.
        preg_match('/clip_id=[0-9]+/', $flash_vars[1], $matches);
        if (!isset($matches[0])) {
          return FALSE;
        }
        // Isolate value for clip_id.
        $vid = str_replace('clip_id=', '', $matches[0]);
      }
      // This must be Vimeo's new iframe embed.
      else {
        $split = preg_split('|/video/|', $url);
        if (!is_array($split)) {
          throw new \UnexpectedValueException('Regular expression error.');
        }
        $vid = $split[1];
        preg_match('/[0-9]+/', $vid, $matches);
        if (!isset($matches[0])) {
          return FALSE;
        }
        $vid = $matches[0];
      }
      if ($vid == '' || !$vid) {
        return FALSE;
      }
      $tmp_dir = \Drupal::service('file_system')->getTempDirectory();
      $filepath = \Drupal::config('system.file')->get('default_scheme') . '://mytube';
      if (file_exists("$filepath/v_$vid.jpg")) {
        return "$filepath/v_$vid.jpg";
      }
      // Fetch an XML file from vimeo.com, which will tell us where the
      // thumbnail is.
      if (!file_exists("$tmp_dir/$vid.xml")) {
        // Download the XML, using clip_id.
        _mytube_download("https://vimeo.com/api/v2/video/$vid.xml", "$tmp_dir/$vid.xml", FALSE);
      }
      $xml = file("$tmp_dir/$vid.xml");
      if (!is_array($xml)) {
        return FALSE;
      }
      // Read downloaded XML, line by line.
      foreach ($xml as $line) {
        if (preg_match('/<thumbnail_large>[\d\D\/_\.]*<\/thumbnail_large>/', $line, $matches)) {
          // Found a thumbnail URL, quit searching.
          break;
        }
      }
      if (!isset($matches[0])) {
        return FALSE;
      }
      $fetch_me = preg_replace('/<\/?thumbnail_large>/', '', $matches[0]);
      break;

    // Unsupported video source.
    default:
      return FALSE;
  }
  $file_dir = \Drupal::config('system.file')->get('default_scheme') . '://mytube';
  \Drupal::service('file_system')->prepareDirectory($file_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
  $filename = $prefix . "_$vid.jpg";
  $filepath = $file_dir . "/$filename";
  if (file_exists($filepath)) {
    // Thumb for this video has already been generated.
    $out = $filepath;
  }
  else {
    // Fetch the file.
    if (!is_string($fetch_me)) {
      throw new \UnexpectedValueException('Regular expression error.');
    }
    $out = _mytube_download($fetch_me, $filepath);
  }
  return $out;
}

/**
 * Given ordinary embed code, try to improve it.
 *
 *  Currently, all this does is add the AutoPlay attribute to YouTube embeds
 *  In the future, other embed code improvements can live in this function too.
 */
function _mytube_fix_embedded(string &$embedded, string $embed_url, string $embed_domain, int $width, int $height): void {
  // Add autoplay attribute.
  switch ($embed_domain) {
    case 'youtube.com':
    case 'youtube-nocookie.com':
      $ytparams = \Drupal::config('mytube.settings')->get('ytparams') ?? '';
      if (!is_string($ytparams)) {
        throw new \UnexpectedValueException('YouTube params must be a string.');
      }
      $ytparams = trim($ytparams);
      $ytparams = str_replace('?', '', $ytparams);
      if (!empty($ytparams) && strpos($ytparams, '&') !== 0) {
        $ytparams = '&' . $ytparams;
      }

      $embedded = _mytube_add_param($embed_url, 'autoplay=1' . $ytparams, $embedded);
      $embedded = str_replace('autoplay=0', 'autoplay=1', $embedded);
      break;

    // cspell:ignore mtvnservices
    case 'mtvnservices.com':
      $embedded = str_replace($embed_url, $embed_url . '&autoPlay=true', $embedded);
      $embedded = str_replace('autoPlay=false', 'autoPlay=true', $embedded);
      break;

    case 'vimeo.com':
      $embedded = _mytube_add_param($embed_url, 'autoplay=1', $embedded);
      break;

    case 'google.com':
      // There are other Google embeds besides videos, so we need to distinguish
      // VIDEO.google.com.
      if (preg_match('/^http:\/\/video.google.com\//', $embed_url, $matches)) {
        $embedded = _mytube_add_param($embed_url, 'autoplay=1', $embedded);
      }
      break;

    // cspell:ignore ebaumsworld
    case 'ebaumsworld.com':
      $embedded = _mytube_add_param($embed_url, 'autostart=true', $embedded);
      break;

    case 'myspace.com':
      $embedded = str_replace($embed_url, $embed_url . ',ap=1', $embedded);
      break;

    // cspell:ignore metacafe
    case 'metacafe.com':
      $embedded = str_replace('autoPlay=no', 'autoPlay=yes', $embedded);
      break;
  }
}

/**
 * Standard method of appending a URL with GET data. Doesn't work in all cases.
 */
function _mytube_add_param(string $url, string $param, string $embed): string {
  // The URL in markup will be HTML-encoded.
  $url = htmlspecialchars($url);
  $param = htmlspecialchars($param);
  if (strpos($url, '?')) {
    return str_replace($url, "$url&amp;$param", $embed);
  }
  else {
    return str_replace($url, "$url?$param", $embed);
  }
}

/**
 * Download a file from $url and store it in $destination.
 *
 * @return string|false
 *   The path of the downloaded thumbnail, or false.
 */
function _mytube_download(string $url, string $destination, bool $preview = FALSE) {
  try {
    $response = \Drupal::httpClient()->request('GET', $url);
  }
  catch (\Exception $e) {
    \Drupal::logger('mytube')->notice('Failed to download file. %error', [
      '%error' => (string) $e,
      'exception' => $e,
    ]);
    return FALSE;
  }
  if ($response->getStatusCode() == 200) {
    // @phpstan-ignore classConstant.deprecated
    return \Drupal::service('file_system')->saveData($response->getBody(), $destination, class_exists(FileExists::class) ? FileExists::Replace : FileSystemInterface::EXISTS_REPLACE);
  }
  return FALSE;
}
