<?php

namespace Drupal\Tests\mytube\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests MyTube module.
 *
 * @group mytube
 */
class MyTubeTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['mytube'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Tests MyTube filter.
   */
  public function testMyTubeFilter(): void {
    $adminUser = $this->drupalCreateUser([
      'administer filters',
      'administer mytube',
      'create article content',
      'use text format full_html',
    ]);
    if ($adminUser) {
      $this->drupalLogin($adminUser);
    }
    $this->drupalGet('admin/config/content/formats/manage/full_html');
    $edit = ['filters[mytube][status]' => TRUE];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('node/add/article');
    $embedCode = '<iframe width="560" height="315" src="https://www.youtube.com/embed/izMGMsIZK4U" frameborder="0" allowfullscreen></iframe>';
    $node = [
      'title[0][value]' => 'testing',
      'body[0][value]' => $embedCode,
      'body[0][format]' => 'full_html',
    ];
    $this->submitForm($node, 'Save');
    $this->assertSession()->responseNotContains($embedCode);
    // cspell:ignore Ciframe Fwww Fembed Fautoplay Fiframe Ciframe
    $expectedMarkup = '<div class="mytube" style="width: 320px;">
  <div class="mytubetrigger" tabindex="0">
    <div class="mytubeembedcode">%3Ciframe%20width%3D%22320%22%20height%3D%22240%22%20src%3D%22https%3A%2F%2Fwww.youtube.com%2Fembed%2FizMGMsIZK4U%3Fautoplay%3D1%22%20frameborder%3D%220%22%20allowfullscreen%3D%22%22%20allow%3D%22autoplay%22%3E%3C%2Fiframe%3E</div>
    <img class="mytubethumb" width="320" height="240" alt="mytubethumb" src="' . $this->container->get('file_url_generator')->generateAbsoluteString('public://mytube/yt_izMGMsIZK4U.jpg') . '" style="margin: 0" loading="lazy" />
    <img class="mytubeplay" alt="play" src="' . $this->container->get('file_url_generator')->generateAbsoluteString($this->container->get('extension.path.resolver')->getPath('module', 'mytube') . '/play.png') . '" style="top: 90px; left: 130px;" />
  </div>
  <div class="mytubetext">
    <span class="mytube-privacy-text">This embed will serve content from <a rel="nofollow noreferrer" href="https://www.youtube.com/embed/izMGMsIZK4U"><em class="placeholder">youtube.com</em></a>.</span>
    <span class="mytube-privacy-link"><a href="https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy" rel="noreferrer" target="_blank">Privacy info.</a></span>
  </div>
</div>';
    try {
      $this->assertSession()->responseContains(str_replace(' />', '>', $expectedMarkup));
    }
    catch (ExpectationException $e) {
      try {
        // @todo Remove when dropping support for 10.1 (XHTML self-closing tags).
        $expected = preg_replace('#/>\s+#', '/>', $expectedMarkup);
        $this->assertIsString($expected);
        $this->assertSession()->responseContains($expected);
      }
      catch (ExpectationException $e) {
        try {
          // @todo Remove when dropping support for 10.0 (no lazy loading filter).
          $expectedMarkup = str_replace(' loading="lazy"', '', $expectedMarkup);
          $this->assertSession()->responseContains($expectedMarkup);
        }
        catch (ExpectationException $e) {
          // @todo Remove when dropping support for older libxml2 versions.
          $expected = preg_replace('#/>\s+#', '/>', $expectedMarkup);
          $this->assertIsString($expected);
          $this->assertSession()->responseContains($expected);
        }
      }
    }
    $this->drupalGet('admin/config/content/mytube');
    $edit = ['allow_custom_size' => TRUE];
    $edit = [
      'allow_custom_size' => TRUE,
      // cspell:ignore Ihre Adresse wird hierbei übermittelt
      'text' => 'Ihre IP-Adresse wird hierbei an !domain übermittelt.',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('node/add/article');
    $this->submitForm($node, 'Save');
    $expectedMarkup = '<div class="mytube" style="width: 560px;">
  <div class="mytubetrigger" tabindex="0">
    <div class="mytubeembedcode">%3Ciframe%20width%3D%22560%22%20height%3D%22315%22%20src%3D%22https%3A%2F%2Fwww.youtube.com%2Fembed%2FizMGMsIZK4U%3Fautoplay%3D1%22%20frameborder%3D%220%22%20allowfullscreen%3D%22%22%20allow%3D%22autoplay%22%3E%3C%2Fiframe%3E</div>
    <img class="mytubethumb" width="560" height="420" alt="mytubethumb" src="' . $this->container->get('file_url_generator')->generateAbsoluteString('public://mytube/yt_izMGMsIZK4U.jpg') . '" style="margin: -52.5px 0" loading="lazy" />
    <img class="mytubeplay" alt="play" src="' . $this->container->get('file_url_generator')->generateAbsoluteString($this->container->get('extension.path.resolver')->getPath('module', 'mytube') . '/play.png') . '" style="top: 127.5px; left: 250px;" />
  </div>
  <div class="mytubetext">
    <span class="mytube-privacy-text">Ihre IP-Adresse wird hierbei an youtube.com übermittelt.</span>
    <span class="mytube-privacy-link"><a href="https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy" rel="noreferrer" target="_blank">Privacy info.</a></span>
  </div>
</div>';
    try {
      $this->assertSession()->responseContains(str_replace(' />', '>', $expectedMarkup));
    }
    catch (ExpectationException $e) {
      try {
        // @todo Remove when dropping support for 10.1 (XHTML self-closing tags).
        $expected = preg_replace('#/>\s+#', '/>', $expectedMarkup);
        $this->assertIsString($expected);
        $this->assertSession()->responseContains($expected);
      }
      catch (ExpectationException $e) {
        try {
          // @todo Remove when dropping support for 10.0 (no lazy loading filter).
          $expectedMarkup = str_replace(' loading="lazy"', '', $expectedMarkup);
          $this->assertSession()->responseContains($expectedMarkup);
        }
        catch (ExpectationException $e) {
          // @todo Remove when dropping support for older libxml2 versions.
          $expected = preg_replace('#/>\s+#', '/>', $expectedMarkup);
          $this->assertIsString($expected);
          $this->assertSession()->responseContains($expected);
        }
      }
    }
  }

}
