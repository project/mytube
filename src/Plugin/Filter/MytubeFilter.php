<?php

namespace Drupal\mytube\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to apply the noreferrer attribute.
 *
 * @Filter(
 *   id = "mytube",
 *   title = @Translation("MyTube Filtering"),
 *   description = @Translation("Replace embeds with clickable thumbnails and caption, preventing remote requests without user consent. Disable the ""Correct faulty and chopped off HTML"" filter to avoid redundant processing."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 10
 * )
 */
class MytubeFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $result->setProcessedText(mytube_process($text));
    $result->setAttachments(['library' => ['mytube/mytube']]);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return _mytube_tips($long);
  }

}
