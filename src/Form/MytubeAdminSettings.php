<?php

namespace Drupal\mytube\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * MyTube settings form.
 */
class MytubeAdminSettings extends ConfigFormBase {

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  final public function __construct(ConfigFactoryInterface $config_factory, ExtensionPathResolver $extension_path_resolver, FileUrlGeneratorInterface $file_url_generator, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('extension.path.resolver'),
      $container->get('file_url_generator'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mytube_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('mytube.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames() {
    return ['mytube.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The settings form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('mytube.settings');
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => "'Privacy info' text",
      '#default_value' => $config->get('text'),
      '#description' => $this->t('Optional: Customize privacy info description below thumbnail. Use <code>!</code><code>embed</code> and <code>!</code><code>domain</code> for embed link and top-level domain, respectively.'),
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => "'Privacy info' link target",
      '#default_value' => $config->get('url'),
      '#description' => $this->t('Specify path where \'Privacy info\' link points to. If left blank, link will point to <a href="@eff" rel="noreferrer">EFF page</a>.', [
        '@eff' => 'https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy',
      ]),
    ];
    $form['ytparams'] = [
      '#type' => 'textfield',
      '#title' => 'Custom YouTube parameters',
      '#default_value' => $config->get('ytparams'),
      '#description' => $this->t('Parameters for <a href="@google" rel="noreferrer">customizing</a> YouTube videos. Note: <code>autoplay=1</code> is added automatically.', [
        '@google' => 'http://code.google.com/apis/youtube/player_parameters.html',
      ]),
    ];
    $form['trusted_domains'] = [
      '#type' => 'textarea',
      '#title' => "Trusted domains",
      '#default_value' => $config->get('trusted_domains'),
      '#description' => $this->t('Domains whose content MyTube should allow through instead of filtering, one per line. For example: example.com, example.org'),
    ];
    $form['default_width'] = [
      '#type' => 'number',
      '#title' => 'Default width',
      '#default_value' => $config->get('default_width'),
      '#description' => $this->t('Width for all MyTube videos and thumbnails. Default is 320 if left empty.'),
      '#size' => 10,
      '#maxlength' => 4,
      '#min' => 0,
    ];
    $form['default_height'] = [
      '#type' => 'number',
      '#title' => 'Default height',
      '#default_value' => $config->get('default_height'),
      '#description' => $this->t('Height for all MyTube videos and thumbnails. Default is 240 if left empty.'),
      '#size' => 10,
      '#maxlength' => 4,
      '#min' => 0,
    ];
    $form['allow_custom_size'] = [
      '#type' => 'checkbox',
      '#title' => 'Users can override default video sizes.',
      '#default_value' => $config->get('allow_custom_size'),
      '#description' => $this->t('If checked, users will be able to set the width and height for <code>&lt;embed&gt;</code>, <code>&lt;iframe&gt;</code> and <code>&lt;object&gt;</code> tags. If not, MyTube will set the width and height attributes of these tags, and their replacement thumbnail images, to the values defined above (or MyTube defaults if not defined).'),
    ];
    $form['allow_remote_thumb'] = [
      '#type' => 'checkbox',
      '#title' => 'Users may embed remote images.',
      '#default_value' => $config->get('allow_remote_thumb'),
      '#description' => $this->t('If checked, users will be able to set <abbr class="mytubehelp" title="Hosted somewhere else, i.e. not on this server.">remote</abbr> thumbs with the <code>thumb=</code> attribute. Since such remote thumbs would send <a href="https://en.wikipedia.org/wiki/HTTP_referer" rel="noreferrer">referer headers</a>, it could defeat the purpose of MyTube. If unchecked, remote images in MyTube objects will be replaced with the <a href="@default">default.gif</a> thumbnail.', [
        '@default' => $this->fileUrlGenerator->generateAbsoluteString($this->extensionPathResolver->getPath('module', 'mytube') . '/default.gif'),
      ]),
    ];
    $form['enable_js_api'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the <a href="https://developers.google.com/youtube/js_api_reference">YouTube Javascript API</a>.'),
      '#default_value' => $config->get('enable_js_api'),
      '#description' => $this->t('If checked, the iFrame Player API code will be loaded asynchronously and the API will be instantiated when a video thumbnail is clicked.'),
    ];
    $form['iframe_max_privacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set <code>&lt;iframe&gt;</code> attributes to maximize privacy'),
      '#default_value' => $config->get('iframe_max_privacy'),
      '#description' => $this->t('If checked, <code>&lt;iframe&gt;</code> <code>allow</code>, <code>credentialless</code>, <code>csp</code>, <code>referrerpolicy</code> and <code>sandbox</code> attributes will be set to maximize user privacy. In addition, YouTube videos will be loaded from <code>www.youtube-nocookie.com</code>.'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
