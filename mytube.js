/* eslint func-names: "off", object-shorthand: "off" */
(function ($, Drupal, once) {
  Drupal.behaviors.mytube = {
    attach: function (context) {
      $(once('mytube-trigger', '.mytubetrigger', context))
        .click(function () {
          $(this).hide();
          $(this).after(unescape($('.mytubeembedcode', this).html()));
          Drupal.attachBehaviors(this);

          // If API usage is enabled, instantiate the API.
          if ($(this).hasClass('mytube-js-api')) {
            Drupal.behaviors.mytube.InitiateYouTubeAPI();
          }
        })
        .keypress(function (e) {
          if (e.which === 13) {
            // Enter key pressed
            $(this).click(); // Trigger search button click event
          }
        });
    },
  };

  /**
   * If API usage is enabled, initialize the player once the API is ready.
   */
  Drupal.behaviors.mytube.InitiateYouTubeAPI = function () {
    if (typeof this.initialized === 'undefined') {
      // Load the iFrame Player API code asynchronously.
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      this.initialized = true;
    }
  };
})(jQuery, Drupal, once);
